package tpnote;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.jupiter.api.Assertions.*;

import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang3.ArrayUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.function.Executable;

public class TestGrp {
	@Test
	public void Testgrp() {
		Sujet s = new Sujet("Réseaux de neurones");
		Groupe p = new Groupe("Groupe 1");
		GestionTER g = new GestionTER();
		g.addGroupe("Groupe 1");
		g.addSujet("Réseaux de neurones");
		p.addVoeu(1,s);
		assertTrue(p.isSujetInVoeu(s)); // on vérifie que le sujet affecté à p est s
		boolean res = false;
		List L;
		L = g.importSujet(); // on récupère la liste des sujets de g
		for (int i = 0 ; i <L.length;i++) {
			if(L[i]==s) {
				res = true;
		assertTrue(res); // on vérifie que le groupe de s est g
	}
	}
}
}
